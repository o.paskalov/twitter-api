<?php

use yii\db\Migration;

/**
 * Class m190304_223920_twitters
 */
class m190304_223920_twitters extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableSchema = Yii::$app->db->schema->getTableSchema('twitters');
        if ($tableSchema === null) {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
            $this->createTable('twitters', [
                'id' => $this->primaryKey(),
                'user' => $this->string()->notNull()->unique()],
            $tableOptions
            );
        }

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190304_223920_twitters cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190304_223920_twitters cannot be reverted.\n";

        return false;
    }
    */
}
