<?php

namespace frontend\controllers;

use app\models\Twitters;
use yii\httpclient\Client;


class TwittersController extends \yii\web\Controller
{

    protected $error_access;
    protected $error_parameter;
    protected $error_internal;

    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionAddTwitter()
    {
        $this->twitterRequest('addTwitter');
    }

    public function actionGetTwitters()
    {
        \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
        $twiusers = Twitters::find()->all();
        if(count($twiusers) > 0 )
        {
            return array('status' => true, 'data'=> $twiusers);
        }
        else
        {
            return array('status'=>false,'data'=> 'No Twitter users found');
        }
    }

    public function actionRemoveTwitter()
    {
        $this->twitterRequest('removeTwitter');
    }

    public function actionFeedTwitter()
    {
        $this->twitterRequest('feedTwitter');
    }

    private function twitterRequest($call)
    {
        $this->error_access = ['error' => 'access denied'];
        $this->error_parameter = ['error' => 'missing paramter'];
        $this->error_internal = ['error' => 'internal erorr'];


        \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;

        if(!empty(\yii::$app->request->get('secret'))){

            $params = \yii::$app->request->get();

            if(empty($params['id'] || $params['user'])) return $this->error_parameter;

            unset($params['secret']);
            if(sha1(implode($params)) === \yii::$app->request->get('secret')){

                return $this->$call();

            }else{
                return $this->error_access;
            }
        }else{
            return $this->error_parameter;
        }
    }

    private function addTwitter()
    {
        $twiuser = new Twitters();
        $twiuser->scenario = Twitters:: SCENARIO_CREATE;
        $twiuser->attributes = ['user' => \yii::$app->request->get('user')];
        if($twiuser->validate())
        {
            $twiuser->save();
            \Yii::$app->response->statusCode = 200;
            \Yii::$app->response->format = \yii\web\Response:: FORMAT_HTML;
            return NULL;
        }
        else
        {
            return $this->error_internal;
        }
    }

    private function removeTwitter()
    {
        $attributes = \yii::$app->request->post();
        $twiuser = Twitters::find()->where(['user' => $attributes['user'] ])->one();
        if(count($twiuser) > 0 )
        {
            $twiuser->delete();
            \Yii::$app->response->statusCode = 200;
            \Yii::$app->response->format = \yii\web\Response:: FORMAT_HTML;
            return NULL;
        }
        else
        {
            return $this->error_internal;
        }
    }

    private function feedTwitter()
    {
        $twiusers = Twitters::find()->all();
        if(count($twiusers) > 0 )
        {


            $consumer_key              = 'qwrwqecrwercgfghhsht';
            $consumer_secret           = 'qwrwqecrwercgfghhsht';
            $oauth_access_token        = 'qwrwqecrwercgfghhsht';
            $oauth_access_token_secret = 'qwrwqecrwercgfghhsht';
            $turl                      = 'https://api.twitter.com/1.1/statuses/user_timeline.json';

            foreach ($twiusers as $tuser){

                $tparams                   = ['screen_name' => $tuser->user, 'exclude_replies' => true];


                $oauth = array(
                    'oauth_consumer_key' => $consumer_key,
                    'oauth_nonce' => time(),
                    'oauth_signature_method' => 'HMAC-SHA1',
                    'oauth_token' => $oauth_access_token,
                    'oauth_timestamp' => time(),
                    'oauth_version' => '1.0'
                );

                $returnp = array();
                ksort($tparams);
                foreach($tparams as $key => $value)
                {
                    $returnp[] = rawurlencode($key) . '=' . rawurlencode($value);
                }
                $base_info = "get&" . rawurlencode($turl) . '&' . rawurlencode(implode('&', $returnp));

                $composite_key = rawurlencode($consumer_secret) . '&' . rawurlencode($oauth_access_token_secret);
                $oauth_signature = base64_encode(hash_hmac('sha1', $base_info, $composite_key, true));
                $oauth['oauth_signature'] = $oauth_signature;


                $auth = 'Authorization: OAuth ';
                $values = array();
                foreach($oauth as $key => $value)
                {
                    if (in_array($key, array('oauth_consumer_key', 'oauth_nonce', 'oauth_signature',
                        'oauth_signature_method', 'oauth_timestamp', 'oauth_token', 'oauth_version'))) {
                        $values[] = "$key=\"" . rawurlencode($value) . "\"";
                    }
                }
                $auth .= implode(', ', $values);

                $twitterHeaders = [$auth, 'Expect:'];

                $client = new Client();
                $response = $client->createRequest()
                    ->setMethod('GET')
                    ->setHeaders($twitterHeaders)
                    ->setUrl($turl)
                    ->setData($tparams)
                    ->send();

                $tweets_set = array($tuser->user => $tweets = $response->data);
            }

            return ['feed'=> $tweets_set];
        }
        else
        {
            return $this->error_internal;
        }
    }

}
